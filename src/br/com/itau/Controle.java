package br.com.itau;

public class Controle {
    private int volume;
    private boolean ligado;
    private boolean tocando;
    private int canal;

    // constructor
    public Controle() {
        this.volume = 50;
        this.ligado = true;
        this.tocando = true;
        this.canal = 2;
    }

    // getters e setters
    public int getVolume() {
        return volume;
    }
    public void setVolume(int volume) {
        this.volume = volume;
    }
    public boolean getLigado() {
        return ligado;
    }
    public void setLigado(boolean ligado) {
        this.ligado = ligado;
    }
    public boolean getTocando() {
        return tocando;
    }
    public void setTocando(boolean tocando) {
        this.tocando = tocando;
    }
    public int getCanal() {
        return canal;
    }
    public void setCanal(int canal) {
        this.canal = canal;
    }

    // métodos
    public void ligar(){
        this.setLigado(true);
    }
    public void desligar(){
        this.setLigado(false);
    }
    public void maisVolume(){
        if(this.getLigado()){
            this.setVolume(this.getVolume() + 1);
        }
    }
    public void menosVolume(){
        if(this.getLigado()){
            this.setVolume(this.getVolume() - 1);
        }
    }
    public void proximoCanal(){
        if(this.getLigado()){
            this.setCanal(this.getCanal() + 1);
        }
    }
    public void canalAnterior(){
        if(this.getLigado()){
            this.setCanal(this.getCanal() - 1);
        }
    }
    public void abrirMenu(){
        System.out.println("Esta ligado: " + this.ligado);
        System.out.println("Esta tocando: " + this.tocando);
        System.out.println("Volume: " + this.volume);
        System.out.println("Canal: " + this.canal);
    }
    public void fecharMenu(){
        System.out.println("Fechando menu....");
    }
}
