package br.com.itau;

public class Main {

    public static void main(String[] args) {
        Controle controle = new Controle();
        controle.setLigado(true);
        controle.maisVolume();
        controle.menosVolume();
        controle.proximoCanal();
        controle.abrirMenu();
        controle.fecharMenu();
    }
}
